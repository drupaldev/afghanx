<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__7d61a4852e89342a42e65ce1214ba026e94ccfaa19e6cf1f2975183d59797194 */
class __TwigTemplate_df571e67aab441a6d9ab7312a47f7b049a212c250f4ec495b0f687ce3f71646b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<img class=\"front_course_list_image img-responsive\" src=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_image"] ?? null)), "html", null, true);
        echo "\" alt =\"\"> 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_code"] ?? null)), "html", null, true);
        echo "</p>


</div>

<div class=\"panel-body\">
<p class=\"course_code\">";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_code"] ?? null)), "html", null, true);
        echo "</p> 

<h4>";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_name"] ?? null)), "html", null, true);
        echo "</h4>

<br>
";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_time_line"] ?? null)), "html", null, true);
        echo " 
<br>
<h5> Starts :  ";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_start"] ?? null)), "html", null, true);
        echo " </h5>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "__string_template__7d61a4852e89342a42e65ce1214ba026e94ccfaa19e6cf1f2975183d59797194";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 20,  86 => 18,  80 => 15,  75 => 13,  66 => 7,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<img class=\"front_course_list_image img-responsive\" src=\"{{ field_course_image }}\" alt =\"\"> 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">{{ field_course_code }}</p>


</div>

<div class=\"panel-body\">
<p class=\"course_code\">{{ field_course_code }}</p> 

<h4>{{ field_course_name }}</h4>

<br>
{{ field_time_line }} 
<br>
<h5> Starts :  {{ field_course_start }} </h5>
</div>
</div>
", "__string_template__7d61a4852e89342a42e65ce1214ba026e94ccfaa19e6cf1f2975183d59797194", "");
    }
}
