<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/contrib/facets/templates/facets-result-item.html.twig */
class __TwigTemplate_aac67ab5e4f609177788e464700f3e57e11b5bd63529ae84fa2a3d2a58e444cb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 29];
        $filters = ["escape" => 30];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 28
        echo "
<div class=\"facet_custom ";
        // line 29
        if (($context["is_active"] ?? null)) {
            echo " facet_active ";
        }
        echo "\">
<span class=\"facet-item__value\">";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["value"] ?? null)), "html", null, true);
        echo "</span>
";
        // line 31
        if (($context["show_count"] ?? null)) {
            // line 32
            echo "  <span class=\"facet-item__count\">(";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["count"] ?? null)), "html", null, true);
            echo ")</span>
";
        }
        // line 34
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "modules/contrib/facets/templates/facets-result-item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 34,  70 => 32,  68 => 31,  64 => 30,  58 => 29,  55 => 28,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation of a facet result item.
 *
 * Available variables:
 * - value: The item value.
 * - raw_value: The raw item value.
 * - show_count: If this facet provides count.
 * - count: The amount of results.
 * - is_active: The item is active.
 * - facet: The facet for this result item.
 *   - id: the machine name for the facet.
 *   - label: The facet label.
 *
 * @ingroup themeable
 */
#}
{#
{% if is_active %}
  <span class=\"facet-item__status js-facet-deactivate\">(-)</span>
{% endif %}
<span class=\"facet-item__value\">{{ value }}</span>
{% if show_count %}
  <span class=\"facet-item__count\">({{ count }})</span>
{% endif %}
#}

<div class=\"facet_custom {% if is_active %} facet_active {% endif %}\">
<span class=\"facet-item__value\">{{ value }}</span>
{% if show_count %}
  <span class=\"facet-item__count\">({{ count }})</span>
{% endif %}

</div>
", "modules/contrib/facets/templates/facets-result-item.html.twig", "/var/www/html/AfghanX/modules/contrib/facets/templates/facets-result-item.html.twig");
    }
}
