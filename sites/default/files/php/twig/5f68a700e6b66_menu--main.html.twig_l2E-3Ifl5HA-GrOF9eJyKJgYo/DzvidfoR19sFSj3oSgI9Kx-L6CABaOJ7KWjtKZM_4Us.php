<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/iitbombayx/templates/menu--main.html.twig */
class __TwigTemplate_473f42f43bd1ce981206d081e52528d12090be06f136db39bb20ab8a0e2a191d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 44, "macro" => 52, "if" => 54, "for" => 60, "set" => 62];
        $filters = ["escape" => 106];
        $functions = ["url" => 126, "link" => 73];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'if', 'for', 'set'],
                ['escape'],
                ['url', 'link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 44
        $context["menus"] = $this;
        // line 45
        echo "
";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["edx_site_path"] ?? null), ($context["logged_username"] ?? null), ($context["logged_email"] ?? null), ($context["profile_link"] ?? null), ($context["logout_link"] ?? null), ($context["account_link"] ?? null)));
        echo "

";
        // line 99
        echo "

<div id=\"after_login_menu\" style=\"display:none;\">
\t<ul class=\"nav navbar-nav nav-right\">
 \t\t<li class=\"dropdown\">
          \t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t<div class=\"logged_user_dropdown\">
                                <div id='username'>";
        // line 106
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logged_username"] ?? null)), "html", null, true);
        echo "</div>

\t\t\t\t<span class=\"caret\"></span>
\t   \t\t        </div>
                        </a>

       \t\t\t   <ul class=\"dropdown-menu\">
            <li><a href=\"";
        // line 113
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["edx_site_path"] ?? null)), "html", null, true);
        echo "/dashboard\">Dashboard</a></li>
            <li><a href=\"";
        // line 114
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["account_link"] ?? null)), "html", null, true);
        echo "\" id=\"account_link\">Account</a></li>
            <li><a href=\"";
        // line 115
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["profile_link"] ?? null)), "html", null, true);
        echo "\" id=\"profile_link\">Profile</a></li>
            <li><a href=\"";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logout_link"] ?? null)), "html", null, true);
        echo "\" id=\"logout_link\">Logout</a></li>
         \t\t    </ul>
 \t\t</li>

\t</ul>
</div> 
<div  id=\"login_tab1\" style=\"display:block;float:right;\">
<ol class=\"menu nav navbar-nav\">
<li>

  <form method=\"get\" action=\"";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>"));
        echo "/courses\" class=\"navbar-form navbar-left\" role=\"search\">
  <div class=\"input-group\">
    <input name=\"search_query\" id=\"search_key_two\" type=\"text\" class=\"form-control\" placeholder=\"Search:\">
  <span class=\"input-group-btn\">
  <button type=\"submit\" id=\"search_course\" class=\"btn btn-warning\"><span class=\"icon fa fa-search\" aria-hidden=\"true\"></span></button>
  </div>
</form>

";
        // line 146
        echo "
      </li>

    </ol>
</div>

";
    }

    // line 52
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__edx_site_path__ = null, $__logged_username__ = null, $__logged_email__ = null, $__profile_link__ = null, $__logout_link__ = null, $__account_link__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "edx_site_path" => $__edx_site_path__,
            "logged_username" => $__logged_username__,
            "logged_email" => $__logged_email__,
            "profile_link" => $__profile_link__,
            "logout_link" => $__logout_link__,
            "account_link" => $__account_link__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 53
            echo "  ";
            $context["menus"] = $this;
            // line 54
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 55
                echo "    ";
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 56
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "menu", 1 => "nav", 2 => "navbar-nav"], "method")), "html", null, true);
                    echo ">
    ";
                } else {
                    // line 58
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "dropdown-menu"], "method")), "html", null, true);
                    echo ">
    ";
                }
                // line 60
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 61
                    echo "      ";
                    // line 62
                    $context["item_classes"] = [0 => (($this->getAttribute(                    // line 63
$context["item"], "is_expanded", [])) ? ("expanded") : ("")), 1 => ((($this->getAttribute(                    // line 64
$context["item"], "is_expanded", []) && (($context["menu_level"] ?? null) == 0))) ? ("dropdown") : ("")), 2 => (($this->getAttribute(                    // line 65
$context["item"], "in_active_trail", [])) ? ("active") : (""))];
                    // line 68
                    echo "      ";
                    if (((($context["menu_level"] ?? null) == 0) && $this->getAttribute($context["item"], "is_expanded", []))) {
                        // line 69
                        echo "        <li";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["item_classes"] ?? null)], "method")), "html", null, true);
                        echo ">
        <a href=\"";
                        // line 70
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), "html", null, true);
                        echo "\" class=\"dropdown-toggle\" data-target=\"#\" data-toggle=\"dropdown\">";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), "html", null, true);
                        echo " <span class=\"caret\"></span></a>
      ";
                    } else {
                        // line 72
                        echo "        <li";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["item_classes"] ?? null)], "method")), "html", null, true);
                        echo ">
        ";
                        // line 73
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", []))), "html", null, true);
                        echo "
      ";
                    }
                    // line 75
                    echo "      ";
                    if ($this->getAttribute($context["item"], "below", [])) {
                        // line 76
                        echo "        ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), $this->getAttribute(($context["attributes"] ?? null), "removeClass", [0 => "nav", 1 => "navbar-nav"], "method"), (($context["menu_level"] ?? null) + 1)));
                        echo "
      ";
                    }
                    // line 78
                    echo "      </li>

    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 81
                echo "
   </ul>


    <div id=\"login_tab\" style=\"display:block;\">
    <ol class=\"right nav-courseware nav navbar-nav navbar-right\">
            <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-default\" href=\"";
                // line 88
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["edx_site_path"] ?? null)), "html", null, true);
                echo "/login\">Login</a>
            </li>
           <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-primary\" href=\"";
                // line 91
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["edx_site_path"] ?? null)), "html", null, true);
                echo "/register\">Register</a>
            </li>
       </ol>
    </div>


  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/iitbombayx/templates/menu--main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 91,  234 => 88,  225 => 81,  217 => 78,  211 => 76,  208 => 75,  203 => 73,  198 => 72,  191 => 70,  186 => 69,  183 => 68,  181 => 65,  180 => 64,  179 => 63,  178 => 62,  176 => 61,  171 => 60,  165 => 58,  159 => 56,  156 => 55,  153 => 54,  150 => 53,  130 => 52,  120 => 146,  109 => 126,  96 => 116,  92 => 115,  88 => 114,  84 => 113,  74 => 106,  65 => 99,  60 => 50,  57 => 45,  55 => 44,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display main menus appear in Header.  *
*                                                                             *
* Created by: Varun Madkaikar & Aditya Datar                                  *
*                                                                             *
*\t\t\t\t\t\t\t\t\t      *
*******************************************************************************
*/

/**
 * @file
 * Default theme implementation to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *
 * @ingroup templates
 */
#}
{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}
{{ menus.menu_links(items, attributes, 0, edx_site_path, logged_username, logged_email, profile_link, logout_link, account_link) }}

{% macro menu_links(items, attributes, menu_level, edx_site_path, logged_username, logged_email, profile_link, logout_link, account_link) %}
  {% import _self as menus %}
  {% if items %}
    {% if menu_level == 0 %}
      <ul{{ attributes.addClass('menu', 'nav', 'navbar-nav') }}>
    {% else %}
      <ul{{ attributes.addClass('dropdown-menu') }}>
    {% endif %}
    {% for item in items %}
      {%
        set item_classes = [
          item.is_expanded? 'expanded',
          item.is_expanded and menu_level == 0 ? 'dropdown',
          item.in_active_trail ? 'active',
        ]
      %}
      {% if menu_level == 0 and item.is_expanded %}
        <li{{ item.attributes.addClass(item_classes) }}>
        <a href=\"{{ item.url }}\" class=\"dropdown-toggle\" data-target=\"#\" data-toggle=\"dropdown\">{{ item.title }} <span class=\"caret\"></span></a>
      {% else %}
        <li{{ item.attributes.addClass(item_classes) }}>
        {{ link(item.title, item.url) }}
      {% endif %}
      {% if item.below %}
        {{ menus.menu_links(item.below, attributes.removeClass('nav', 'navbar-nav'), menu_level + 1) }}
      {% endif %}
      </li>

    {% endfor %}

   </ul>


    <div id=\"login_tab\" style=\"display:block;\">
    <ol class=\"right nav-courseware nav navbar-nav navbar-right\">
            <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-default\" href=\"{{ edx_site_path }}/login\">Login</a>
            </li>
           <li class=\"nav-courseware-01\">
              <a type=\"button\" class=\"btn btn-primary\" href=\"{{ edx_site_path }}/register\">Register</a>
            </li>
       </ol>
    </div>


  {% endif %}
{% endmacro %}


<div id=\"after_login_menu\" style=\"display:none;\">
\t<ul class=\"nav navbar-nav nav-right\">
 \t\t<li class=\"dropdown\">
          \t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t<div class=\"logged_user_dropdown\">
                                <div id='username'>{{ logged_username }}</div>

\t\t\t\t<span class=\"caret\"></span>
\t   \t\t        </div>
                        </a>

       \t\t\t   <ul class=\"dropdown-menu\">
            <li><a href=\"{{ edx_site_path }}/dashboard\">Dashboard</a></li>
            <li><a href=\"{{ account_link }}\" id=\"account_link\">Account</a></li>
            <li><a href=\"{{ profile_link }}\" id=\"profile_link\">Profile</a></li>
            <li><a href=\"{{ logout_link }}\" id=\"logout_link\">Logout</a></li>
         \t\t    </ul>
 \t\t</li>

\t</ul>
</div> 
<div  id=\"login_tab1\" style=\"display:block;float:right;\">
<ol class=\"menu nav navbar-nav\">
<li>

  <form method=\"get\" action=\"{{ url('<front>') }}/courses\" class=\"navbar-form navbar-left\" role=\"search\">
  <div class=\"input-group\">
    <input name=\"search_query\" id=\"search_key_two\" type=\"text\" class=\"form-control\" placeholder=\"Search:\">
  <span class=\"input-group-btn\">
  <button type=\"submit\" id=\"search_course\" class=\"btn btn-warning\"><span class=\"icon fa fa-search\" aria-hidden=\"true\"></span></button>
  </div>
</form>

{#
          <form method=\"get\" action=\"{{ url('<front>') }}/courses\" >
                                          <section class=\"wrapper_special course-search\">

                                              <div style=\"float:left;width:100%;\"><input name=\"search_query\" class='temptext' id=\"search_key_two\" type=\"text\" ></input></div>
                                              <div style=\"float:left;width:0%;\"><button type=\"submit\" id=\"search_course\">
                                              <span class=\"icon fa fa-search\" aria-hidden=\"true\"></span>
                                              </button></div>
                                          </section>
                                      </form>

#}

      </li>

    </ol>
</div>

", "themes/custom/iitbombayx/templates/menu--main.html.twig", "/var/www/html/AfghanX/themes/custom/iitbombayx/templates/menu--main.html.twig");
    }
}
