<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__b74ca359bb2b981638314db60f22ffb3a7a3164ebab2ec8b07e6120071d1b912 */
class __TwigTemplate_9869d3189e5c97a34c54253ea45a21b43914fc0449230c55ae3fbffeb8dd4f3f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<!-- <img  class=\"img-responsive\" src=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_image"] ?? null)), "html", null, true);
        echo "\" alt =\"\" style=\"height:142px;width:378px;\"> -->

<img  class=\"course_list_image img-responsive\" src=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_image"] ?? null)), "html", null, true);
        echo "\" > 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_code"] ?? null)), "html", null, true);
        echo "</p>


</div>

<div class=\"panel-body\" style=\"max-height:225px;padding-bottom:0px;\">
<p class=\"course_code\">";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_code"] ?? null)), "html", null, true);
        echo "</p> 

<h4>";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_name"] ?? null)), "html", null, true);
        echo "</h4>


<div class=\"wtf\">
<div  class=\"course_card_strat_date\">

";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_time_line_1"] ?? null)), "html", null, true);
        echo " 

<br>
<h5> Starts :  ";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_course_start"] ?? null)), "html", null, true);
        echo " </h5>
</div>


<div  class=\"course_card_mooc_icon\">";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_mooc_icon"] ?? null)), "html", null, true);
        echo "</div>

</div>

</div>



</div>

";
    }

    public function getTemplateName()
    {
        return "__string_template__b74ca359bb2b981638314db60f22ffb3a7a3164ebab2ec8b07e6120071d1b912";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 30,  100 => 26,  94 => 23,  85 => 17,  80 => 15,  71 => 9,  64 => 5,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<!-- <img  class=\"img-responsive\" src=\"{{ field_course_image }}\" alt =\"\" style=\"height:142px;width:378px;\"> -->

<img  class=\"course_list_image img-responsive\" src=\"{{ field_course_image }}\" > 
<div class=\"course_image_hover\"><p> Learn More</p></div>


<p class=\"clickable_panel\">{{ field_course_code }}</p>


</div>

<div class=\"panel-body\" style=\"max-height:225px;padding-bottom:0px;\">
<p class=\"course_code\">{{ field_course_code }}</p> 

<h4>{{ field_course_name }}</h4>


<div class=\"wtf\">
<div  class=\"course_card_strat_date\">

{{ field_time_line_1 }} 

<br>
<h5> Starts :  {{ field_course_start }} </h5>
</div>


<div  class=\"course_card_mooc_icon\">{{ field_mooc_icon }}</div>

</div>

</div>



</div>

", "__string_template__b74ca359bb2b981638314db60f22ffb3a7a3164ebab2ec8b07e6120071d1b912", "");
    }
}
