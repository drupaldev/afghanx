<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig */
class __TwigTemplate_121111ec3e544a528e1dcbec64d3d7a8709345bf51d39f1748270577ad22b8a7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 25, "set" => 26, "for" => 49];
        $filters = ["escape" => 32, "length" => 35, "raw" => 59];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set', 'for'],
                ['escape', 'length', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 24
        echo "
";
        // line 25
        if ($this->getAttribute(($context["context"] ?? null), "list_style", [])) {
            // line 26
            $context["attributes"] = $this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ("item-list__" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["context"] ?? null), "list_style", [])))], "method");
        }
        // line 29
        if ((($context["items"] ?? null) || ($context["empty"] ?? null))) {
            // line 31
            if ( !twig_test_empty(($context["title"] ?? null))) {
                // line 32
                echo "<h3>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
                echo "</h3>";
            }
            // line 35
            if (((twig_length_filter($this->env, ($context["items"] ?? null)) >= 1) || (twig_length_filter($this->env, ($context["search_query"] ?? null)) > 0))) {
                // line 36
                echo "<span class=\"selected_filters_txt\">Selected Filters: </span>
   <span class=\"selected_filters_tabs\">




    <";
                // line 42
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["list_type"] ?? null)), "html", null, true);
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null)), "html", null, true);
                echo ">";
                // line 44
                if ((twig_length_filter($this->env, ($context["search_query"] ?? null)) > 0)) {
                    // line 45
                    echo "<li><a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["newlink"] ?? null)), "html", null, true);
                    echo "\"> ";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["search_query"] ?? null)), "html", null, true);
                    echo "</a></li>";
                }
                // line 49
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 50
                    echo "<li";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "attributes", [])), "html", null, true);
                    echo ">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "value", [])), "html", null, true);
                    echo "</li>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "<li><a href=\"/courses\" rel=\"nofollow\">Reset All</a></li>
    </";
                // line 54
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["list_type"] ?? null)), "html", null, true);
                echo ">
  </span>";
            } else {
                // line 57
                echo "<span class=\"selected_filters_txt\">Selected Filters: </span>
    <span class=\"default_filters_tabs\">
     ";
                // line 59
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["empty"] ?? null)));
                echo "
    </span>
  </span>";
            }
        }
    }

    public function getTemplateName()
    {
        return "modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 59,  117 => 57,  112 => 54,  109 => 53,  99 => 50,  95 => 49,  88 => 45,  86 => 44,  82 => 42,  74 => 36,  72 => 35,  67 => 32,  65 => 31,  63 => 29,  60 => 26,  58 => 25,  55 => 24,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation for a facets summary item list.
 *
 * Available variables:
 * - items: A list of items. Each item contains:
 *   - attributes: HTML attributes to be applied to each list item.
 *   - value: The content of the list element.
 * - title: The title of the list.
 * - list_type: The tag for list element (\"ul\" or \"ol\").
 * - wrapper_attributes: HTML attributes to be applied to the list wrapper.
 * - attributes: HTML attributes to be applied to the list.
 * - empty: A message to display when there are no items. Allowed value is a
 *   string or render array.
 * - context: A list of contextual data associated with the list. May contain:
 *   - list_style: The custom list style.
 *
 * @see facets_summary_preprocess_facets_summary_item_list()
 *
 * @ingroup themeable
 */
#}

{% if context.list_style %}
  {%- set attributes = attributes.addClass('item-list__' ~ context.list_style) %}
{% endif %}

{%- if items or empty -%}

  {%- if title is not empty -%}
    <h3>{{ title }}</h3>
  {%- endif -%}

  {%- if items|length >= 1 or search_query|length > 0 -%}
   <span class=\"selected_filters_txt\">Selected Filters: </span>
   <span class=\"selected_filters_tabs\">




    <{{ list_type }}{{ attributes }}>

      {%- if search_query|length > 0 -%}
        <li><a href=\"{{ newlink }}\"> {{ search_query }}</a></li>
      {%- endif -%}


      {%- for item in items -%}
        <li{{ item.attributes }}>{{ item.value }}</li>
      {%- endfor -%}

       <li><a href=\"/courses\" rel=\"nofollow\">Reset All</a></li>
    </{{ list_type }}>
  </span>
  {%- else -%}
  <span class=\"selected_filters_txt\">Selected Filters: </span>
    <span class=\"default_filters_tabs\">
     {{ empty|raw }}
    </span>
  </span>
  {%- endif -%}

{%- endif -%}

", "modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig", "/var/www/html/AfghanX/modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig");
    }
}
