<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__e96fb3256db698305e30ac16e0f073a73e33cde3c271a8c040e4ae3e6e3c31e0 */
class __TwigTemplate_597332320d97e92d6c592306faf70854e12090d42f82d11c8d3deb432d4c0033 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1];
        $filters = ["escape" => 2];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (twig_test_empty(($context["field_link_"] ?? null))) {
            // line 2
            echo " <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_1179_x_359_"] ?? null)), "html", null, true);
            echo "\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_970_x_359_"] ?? null)), "html", null, true);
            echo "\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_750_x_359_"] ?? null)), "html", null, true);
            echo "\">
";
        } else {
            // line 4
            echo "   <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_link_"] ?? null)), "html", null, true);
            echo "\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_1179_x_359_"] ?? null)), "html", null, true);
            echo "\"></a><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_link_"] ?? null)), "html", null, true);
            echo "\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_970_x_359_"] ?? null)), "html", null, true);
            echo "\"></a><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_link_"] ?? null)), "html", null, true);
            echo "\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_slide_image_750_x_359_"] ?? null)), "html", null, true);
            echo "\"></a>

";
        }
    }

    public function getTemplateName()
    {
        return "__string_template__e96fb3256db698305e30ac16e0f073a73e33cde3c271a8c040e4ae3e6e3c31e0";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 4,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}{% if field_link_ is empty  %}
 <img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\">
{% else %}
   <a href=\"{{ field_link_ }}\"><img alt=\"img1\" class=\"img-responsive hidden-md hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"1f2b7643-1cc3-4162-a5fa-c57fd89f694a\" src=\"{{ field_slide_image_1179_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img2\" class=\"img-responsive hidden-lg hidden-sm hidden-xs\" data-entity-type=\"file\" data-entity-uuid=\"04c5ee0f-6dc7-4143-a798-6d6ade284969\" src=\"{{ field_slide_image_970_x_359_ }}\"></a><a href=\"{{ field_link_ }}\"><img alt=\"img3\" class=\"img-responsive hidden-md hidden-lg\" data-entity-type=\"file\" data-entity-uuid=\"24ff748d-9f21-42bf-9921-90cf0680de00\" src=\"{{ field_slide_image_750_x_359_ }}\"></a>

{% endif %}", "__string_template__e96fb3256db698305e30ac16e0f073a73e33cde3c271a8c040e4ae3e6e3c31e0", "");
    }
}
