<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig */
class __TwigTemplate_2200e2aef9fe2d6229cab45c8bbe8c19dd2ba332d2abe800c4a9bed33edb51af extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["for" => 46, "if" => 48];
        $filters = ["trim" => 48, "render" => 48, "escape" => 57];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if'],
                ['trim', 'render', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 45
        echo "
";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 47
            echo "
";
            // line 48
            if ((twig_trim_filter($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute($context["row"], "content", []))) == "")) {
            } else {
                // line 50
                echo "<section id=\"block-announcement\" class=\"block block-block-content clearfix\">
   <div class=\"alert alert-info alert-dismissible\" role=\"alert\" style=\"background-color: #fffacd; padding-top:0px;\">
     <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\"><span aria-hidden=\"true\">×</span>
     </button>
     <div class=\"text-center\">
\t\t<h6><span aria-hidden=\"true\" class=\"glyphicon glyphicon-bullhorn\"></span> Announcement</h6>

";
                // line 57
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["row"], "content", [])), "html", null, true);
                echo "


</div>

</div>
      
  </section>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "themes/custom/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 57,  68 => 50,  65 => 48,  62 => 47,  58 => 46,  55 => 45,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#

/**
* This file is part of IITBombayX-Drupal.
*
* IITBombayX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IITBombayX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This customised view file is created for the display Announcement. *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
*******************************************************************************
*/


/**
 * @file 
 * Theme override to display a view of unformatted rows.
 *
 * Available variables:
 * - title: The title of this group of rows. May be empty.
 * - rows: A list of the view's row items.
 *   - attributes: The row's HTML attributes.
 *   - content: The row's content.
 * - view: The view object.
 * - default_row_class: A flag indicating whether default classes should be
 *   used on rows.
 *
 * @see template_preprocess_views_view_unformatted()
 */
#}

{% for row in rows %}

{% if row.content|render|trim == ''   %}
{% else %}
<section id=\"block-announcement\" class=\"block block-block-content clearfix\">
   <div class=\"alert alert-info alert-dismissible\" role=\"alert\" style=\"background-color: #fffacd; padding-top:0px;\">
     <button aria-label=\"Close\" class=\"close\" data-dismiss=\"alert\" type=\"button\"><span aria-hidden=\"true\">×</span>
     </button>
     <div class=\"text-center\">
\t\t<h6><span aria-hidden=\"true\" class=\"glyphicon glyphicon-bullhorn\"></span> Announcement</h6>

{{ row.content }}


</div>

</div>
      
  </section>
{% endif %}
{% endfor %}
", "themes/custom/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig", "/var/www/html/AfghanX/themes/custom/iitbombayx/templates/views-view-unformatted--announcement_section.html.twig");
    }
}
