/**
 * @file
 * Attaches behaviors for the Clientside Validation jQuery module.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
        $("#messageDiv").parent().hide();
	localStorage.clear();
	$( 'label[for="edit-message-0-value"]' ).after( "<div class='message_desc'>The more you tell us, the more quickly and helpfully we can respond!</div>" );

  if (typeof drupalSettings.cvJqueryValidateOptions === 'undefined') {
    drupalSettings.cvJqueryValidateOptions = {};
  }
  if (drupalSettings.clientside_validation_jquery.force_validate_on_blur) {
    drupalSettings.cvJqueryValidateOptions.onfocusout = function (element) {
      // "eager" validation
      this.element(element);
    };
 }

    drupalSettings.cvJqueryValidateOptions.errorLabelContainer= "#messageBox";
    drupalSettings.cvJqueryValidateOptions.errorElement= "span";
    drupalSettings.cvJqueryValidateOptions.wrapper='li';

    drupalSettings.cvJqueryValidateOptions.highlight= function (element, errorClass) {
        $(element).closest('.form-group').addClass('has-error');
	    $('#messageDiv').parent().show();
	    $('#messageDiv').show();
    };
    drupalSettings.cvJqueryValidateOptions.unhighlight = function (element, errorClass) {
        $(element).closest('.form-group').removeClass('has-error');
	   if( $('.has-error').length == 0) $('#messageDiv').parent().hide(); 
    };
    drupalSettings.cvJqueryValidateOptions.rules= {
            name: {
                required: true,
                minlength:5,
		maxlength:65,
		lettersonly:true,
            },
            mail: {
                required: true,
		  emailx: true,
            },
	    'subject[0][value]': {
                required: true,
		minlength:10,
		maxlength:70,
		alphanumeric:true,
            },
            'message[0][value]': {
                required: true,
		minlength:140,
		maxlength:6000,
            }
        };


    drupalSettings.cvJqueryValidateOptions.messages= {
            name: {
                required:'Enter valid name',
                minlength:'Enter valid name',
                maxlength:'Enter valid name',
                lettersonly:'Enter valid name',
            },
            mail: {
                required: 'Enter valid email',
                  emailx: 'Enter valid email',
            },
            'subject[0][value]': {
                required: 'Enter valid subject',
                minlength:'Enter valid subject',
                maxlength: 'Enter valid subject' ,
		alphanumeric: 'Enter valid subject',
            },
            'message[0][value]': {
                required: 'Enter valid message',
                minlength: 'Enter valid message',
                maxlength: 'Enter valid message',
            }
        };


   /* drupalSettings.cvJqueryValidateOptions.showErrors = function(errorMap, errorList) {
    var summary = "You have the following errors: \n";
    $.each(errorList, function() { summary += "  " + this.message + "\n"; });
    $("#messageBox").html(summary);
    $.each(errorList, function() { this.element.css("border", "1px solid red"); });
         this.defaultShowErrors();
  };*/

$.validator.addMethod( "emailx", function( value, element ) {
        return this.optional( element ) || /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test( value )

}, "Enter valid email" );

$.validator.addMethod( "lettersonly", function( value, element ) {
	return this.optional( element ) || /^[a-z\s]+$/i.test( value );
}, "Letters only please" );  

$.validator.addMethod( "alphanumeric", function( value, element ) {
	return this.optional( element ) || /^[\w\s]+$/i.test( value );
}, "Letters, numbers, and underscores only please" );
  // Add messages with translations from backend.
// $.extend($.validator.messages, drupalSettings.clientside_validation_jquery.messages);

  // Allow all modules to update the validate options.
  // Example of how to do this is shown below.
  $(document).trigger('cv-jquery-validate-options-update', drupalSettings.cvJqueryValidateOptions);

  /**
   * Attaches jQuery validate behavior to forms.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Attaches the outline behavior to the right context.
   */
  Drupal.behaviors.cvJqueryValidate = {
    attach: function (context) {
      if (typeof Drupal.Ajax !== 'undefined') {
        // Update Drupal.Ajax.prototype.beforeSend only once.
        if (typeof Drupal.Ajax.prototype.beforeSubmitCVOriginal === 'undefined') {
          var validateAll = 2;
          try {
            validateAll = drupalSettings.clientside_validation_jquery.validate_all_ajax_forms;
          }
          catch(e) {
            // Do nothing if we do not have settings or value in settings.
          }

          Drupal.Ajax.prototype.beforeSubmitCVOriginal = Drupal.Ajax.prototype.beforeSubmit;
          Drupal.Ajax.prototype.beforeSubmit = function (form_values, element_settings, options) {
            if (typeof this.$form !== 'undefined' && (validateAll === 1 || $(this.element).hasClass('cv-validate-before-ajax'))) {
              $(this.$form).removeClass('ajax-submit-prevented');

              $(this.$form).validate();
              if (!($(this.$form).valid())) {
                this.ajaxing = false;
                $(this.$form).addClass('ajax-submit-prevented');
                return false;
              }
            }

            return this.beforeSubmitCVOriginal.apply(this, arguments);
          };
        }
      }

      $(context).find('form').once('cvJqueryValidate').each(function() {
        $(this).validate(drupalSettings.cvJqueryValidateOptions);
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
